const express = require("express");
const PORT = 3000;

const datatime_checker = require("./handlers/datetime_checker/datetime_checker");
const slack_send_reminder = require("./handlers/slack/message_send/slack_send_reminder");

global.emails_sent_for_datetime = [];
const datetimes_to_fire = {
  days: ["Monday", "Wednesday", "Friday"],
  times: ["01:01 am", "11:01 am", "23:00 pm"],
};

datatime_checker(slack_send_reminder, 500, datetimes_to_fire);

const App = express();

//server connection
App.listen(PORT, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("App is connected to port " + PORT);
  }
});
