//this file will constantly check the datetime to see if it matches the input days and times
/*Implementation
global.emails_sent_for_datetime = []
const datetimes_to_fire = {
  days: ["Monday", "Wednesday", "Friday", "Saturday"],
  times: [
    '08:11 am',
    '08:13 am',
    '11:01 am',
    '23:00 pm']
}
datatime_checker(slack_send_reminder, 500,datetimes_to_fire)
*/

const getData = require("../getData/getData");
const send_mail = require("../email/email_send")

function check_day(function_to_run_when_datetime_matches, datetimes_to_fire) {
  var now = new Date();
  var days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  var months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  var time = now.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" });
  var day = days[now.getDay()];
  var month = months[now.getMonth()];
  if (datetimes_to_fire.days.includes(day)) {
    check_time(function_to_run_when_datetime_matches, time, datetimes_to_fire);
  } else {
    global.emails_sent_for_datetime = [];
  }
}

function check_time(
  function_to_run_when_datetime_matches,
  time,
  datetimes_to_fire
) {
  if (datetimes_to_fire.times.includes(time)) {
    if (!global.emails_sent_for_datetime.includes(time)) {
      global.emails_sent_for_datetime = datetimes_to_fire.times;
      function_to_run_when_datetime_matches(); // this is the origanal function passed to datetime checker
      getData(function (response) {
        let unique = new Set();
        response.map((mail)=>{
        unique.add(mail.personal_email)
    })

    console.log(unique)
    for (let mail of unique){
      //send_mail(mail)
      console.log("Email sent to" + mail)
    }
        //we have access to array data here
      }); //get data from database for people who have missed report today
    }
  } else {
    global.emails_sent_for_datetime = [];
    console.log("Not reporting time: ", time);
  }
}

function repeat_checks(
  function_to_run_when_datetime_matches,
  func,
  timeout,
  datetimes_to_fire
) {
  //this is in a constant loop. runs a function each timeout
  func(datetimes_to_fire);
  setTimeout(
    () =>
      repeat_checks(
        function_to_run_when_datetime_matches,
        func,
        timeout,
        datetimes_to_fire
      ),
    timeout
  );
}

var main = function (
  function_to_run_when_datetime_matches,
  time_interval_to_check,
  datetimes_to_fire
) {
  repeat_checks(
    function_to_run_when_datetime_matches,
    () => check_day(function_to_run_when_datetime_matches, datetimes_to_fire),
    time_interval_to_check,
    datetimes_to_fire
  );
};

module.exports = main;
