const mysql = require("mysql");

const getData = (callback) => {
  //create connection need to modified as per our DB connection
  const db = mysql.createConnection({
    // host: "localhost",
    // user: "root",
    // password: "",
    // database: "test1611",
    host: "mysql.dev.utopiatech.io",
    user: "devmainuser",
    password: "passpass",
    database: "utopiatechdevdb",
  });

  //connect and check
  db.connect((err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("DB is connected");
    }
  });

  var date2 = new Date();

  //let sql = "SELECT * FROM reports";
  let sql = `SELECT applicants.personal_email FROM applicants 
  JOIN employees ON applicants.applicant_id = employees.application_id JOIN reports 
  ON reportee_payroll_id = employees.payroll_id and reports.report_datetime_created != ${date2.getUTCDate()}`

  db.query(sql, (err, result) => {
    const array = []; //to store the data from database
    
    if (err) throw err;
    result.map((data) => {
      //console.log(data);
        array.push(data);
    });
    db.end();
    console.log("db disconnected")
    return callback(array);
    //send mail to every person in array
  });
  
};
module.exports = getData;
