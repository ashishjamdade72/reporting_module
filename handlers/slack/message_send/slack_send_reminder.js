var main = function (data) {
  console.log("Sending Reporting Day Reminder");
  const axios = require("axios");
  run().catch((err) => console.log(err));
  async function run() {
    const url =
      "https://hooks.slack.com/services/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";
    const res = await axios.post(url, {
      blocks: [
        {
          type: "header",
          text: {
            type: "plain_text",
            text:
              ":red_circle::red_circle::red_circle: Reporting Day :red_circle::red_circle::red_circle:",
            emoji: true,
          },
        },
        {
          type: "header",
          text: {
            type: "plain_text",
            text: "All TEAM MEMBERS",
            emoji: true,
          },
        },
        {
          type: "divider",
        },
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text:
              "*Submit your report today.*\n\n*All team need to ensure reports are done on reporting days Brisbane time.*",
          },
        },
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: "*Reporting Days:* Monday, Wednesday & Friday",
          },
        },
        {
          type: "divider",
        },
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: ":fire: *3 missed reporting days = Termination* :fire:",
          },
        },
        {
          type: "divider",
        },
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text:
              "1. Login to the HR App. https:www.hr.utopiatech.io/login \n2. Your login detailswere emailed out to you.\n3. If you forgot your password, use the reset passwords.\n4. If you forgot your team member id, contact HR  all-team\n5. Go to create report page. https:www.hr.utopiatech.io/Report_Create\n6. Fill out report.\n7. Press submit and wait or confirmation popup",
          },
        },
        {
          type: "context",
          elements: [
            {
              type: "mrkdwn",
              text: "Managment",
            },
          ],
        },
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: "@channel",
          },
        },
      ],
    });

    console.log("Slack Message sent"); //, res.data);
  }
};

module.exports = main;
